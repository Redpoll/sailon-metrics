# SAIL-ON metrics

Implemented in rust and python

## Install

```
$ pip install -e .
```

## Example

```python
from sailon_metrics import m1

pred = [
    [False, False,  True, True],
    [False, False, False, True],
]

truth = [
    [False,  True, True, True],
    [False, False, True, True],
]

m1(pred, truth)
```
