use pyo3::prelude::*;
use sailon_metrics_core::*;

#[pymodule]
fn sailon_metrics(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    #[pyfn(m, "m1")]
    fn m1_py(preds: Vec<Vec<bool>>, truths: Vec<Vec<bool>>) -> PyResult<f64> {
        Ok(m1(&preds, &truths).unwrap())
    }

    #[pyfn(m, "m2")]
    fn m2_py(preds: Vec<Vec<bool>>, truths: Vec<Vec<bool>>) -> PyResult<f64> {
        Ok(m2(&preds, &truths).unwrap())
    }

    #[pyfn(m, "m2_1")]
    fn m2_1_py(preds: Vec<Vec<bool>>, truths: Vec<Vec<bool>>) -> PyResult<f64> {
        Ok(m2_1(&preds, &truths).unwrap())
    }

    Ok(())
}
