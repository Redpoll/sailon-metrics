use thiserror::Error;

pub struct ConfusionMatrix {
    pub t_pos: u32,
    pub f_pos: u32,
    pub f_neg: u32,
    pub t_neg: u32,
}

#[derive(Debug, Clone, Error)]
pub enum ConfusionMatrixError {
    #[error("preds has {preds_len} entries but truth has {truth_len} entries")]
    PredsAndTruthMisaligned { preds_len: usize, truth_len: usize },
}

impl ConfusionMatrix {
    pub fn new() -> Self {
        ConfusionMatrix {
            t_pos: 0,
            f_pos: 0,
            f_neg: 0,
            t_neg: 0,
        }
    }

    pub fn from_vecs<T: Into<bool> + Copy>(
        preds: &Vec<T>,
        truth: &Vec<T>,
    ) -> Result<Self, ConfusionMatrixError> {
        let preds_len = preds.len();
        let truth_len = truth.len();
        if preds_len != truth_len {
            return Err(ConfusionMatrixError::PredsAndTruthMisaligned {
                preds_len,
                truth_len,
            });
        }

        let mut confmat = ConfusionMatrix::new();
        preds.iter().zip(truth.iter()).for_each(|(&p, &t)| {
            let pred: bool = p.into();
            let tru: bool = t.into();
            if pred {
                if tru {
                    confmat.t_pos += 1;
                } else {
                    confmat.f_pos += 1;
                }
            } else {
                if tru {
                    confmat.f_neg += 1;
                } else {
                    confmat.t_neg += 1;
                }
            }
        });
        Ok(confmat)
    }
}

#[derive(Debug, Clone, Error)]
pub enum MetricError {
    #[error("Cannot take average of zero elements")]
    AverageOfZeroElements,
    #[error("preds has {preds_len} trials but truths has {truths_len} trials")]
    PredsAndTruthsMisaligned { preds_len: usize, truths_len: usize },
    #[error("No trials provided")]
    ZeroTrials,
}

fn validate_preds_and_truths<T>(
    preds: &Vec<Vec<T>>,
    truths: &Vec<Vec<T>>,
) -> Result<(), MetricError> {
    if preds.len() != truths.len() {
        Err(MetricError::PredsAndTruthsMisaligned {
            preds_len: preds.len(),
            truths_len: truths.len(),
        })
    } else if preds.is_empty() {
        Err(MetricError::ZeroTrials)
    } else {
        Ok(())
    }
}

pub fn m1<T: Into<bool> + Copy>(
    preds: &Vec<Vec<T>>,
    truths: &Vec<Vec<T>>,
) -> Result<f64, Box<dyn std::error::Error>> {
    validate_preds_and_truths(preds, truths)?;
    let mut n: u32 = 0;
    let sum = preds
        .iter()
        .zip(truths.iter())
        .try_fold(0.0, |acc, (pred, truth)| {
            ConfusionMatrix::from_vecs(pred, truth).map(|confmat| {
                if confmat.t_pos > 0 && confmat.f_pos == 0 {
                    n += 1;
                    acc + f64::from(confmat.f_neg)
                } else {
                    acc
                }
            })
        })?;

    if n > 0 {
        Ok(sum / f64::from(n))
    } else {
        Err(MetricError::AverageOfZeroElements.into())
    }
}

pub fn m2<T: Into<bool> + Copy>(
    preds: &Vec<Vec<T>>,
    truths: &Vec<Vec<T>>,
) -> Result<f64, Box<dyn std::error::Error>> {
    validate_preds_and_truths(preds, truths)?;
    // count the number of trials with at least on TP and no FP
    let count = preds
        .iter()
        .zip(truths.iter())
        .try_fold(0.0, |acc, (pred, truth)| {
            ConfusionMatrix::from_vecs(pred, truth).map(|confmat| {
                if confmat.t_pos > 0 && confmat.f_pos == 0 {
                    acc + 1.0
                } else {
                    acc
                }
            })
        })?;

    // turn count into a percent
    if count > 0.0 {
        Ok(count / (preds.len() as f64) * 100.0)
    } else {
        Err(MetricError::AverageOfZeroElements.into())
    }
}

/// Percent of Trials with at least one FP
pub fn m2_1<T: Into<bool> + Copy>(
    preds: &Vec<Vec<T>>,
    truths: &Vec<Vec<T>>,
) -> Result<f64, Box<dyn std::error::Error>> {
    // count the number of trials with at least one FP
    let count = preds
        .iter()
        .zip(truths.iter())
        .try_fold(0.0, |acc, (pred, truth)| {
            ConfusionMatrix::from_vecs(pred, truth).map(|confmat| {
                if confmat.f_pos > 0 {
                    acc + 1.0
                } else {
                    acc
                }
            })
        })?;

    // turn count into a percent
    if count > 0.0 {
        Ok(count / (preds.len() as f64) * 100.0)
    } else {
        Err(MetricError::AverageOfZeroElements.into())
    }
}
